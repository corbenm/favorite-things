Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
orange
***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
pasta

3. Who is your favorite fictional character?
Geralt of Rivia

4. What is your favorite animal?
Grizzly Bear

5. What is your favorite programming language? (Hint: You can always say Python!!)
its not "C" is the best by far
